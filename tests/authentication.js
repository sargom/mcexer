"use strict";

var LoginPage = require("./page-objects/login.page.js");
var InboxPage = require("./page-objects/inbox.page.js");
var URL = "https://login-alpha.mimecast.com/m/secure/login/?tkn=WVXYHQ7cA8nSKgxK0RS4nkdOIDqpeB5cYTr3ptKQOtRRUm0poN7PZ_cOXXqmu1ZSPxbDSpccdqyHcLr4Z0WYoUUaJR4MrVGHESpRXK3_Gyw#/login?message=eNo1jm1LwzAURv9Lvrpo0qRZO0ScuoEWRQYqDKGk6W2tzYukacGK_91s6ufnnnPuFxpAjR66Gq0QqIFs2kk-Wr1liZ-9cVSQ4vbGz7naUr1fa1UnxXsfqs3VJHfu2e3h5Y41iX09q0-sRQtkYBhkC9q5fvwIrgeLVnbUeoGCCQ-uhtghhMZLqUI3_Yav6ZqWOeVZJvLynDecsIqkOBGsxryRFa6YYBikUHGTXDZwEQUT-KFz0U-P8ns32oNs7AN4Qw4Fr97-E5td-VTwFNOsPNYSQpeE07QU5R-A41s4Z0sqch5hMLLTERykl5-tMzAb6UNnL9vDcKqcQd8_rKRkZQ";

describe("Mimecast Login Exercise", function() {
  var login = new LoginPage();
  var inbox = new InboxPage();

  describe("#Case 1", function() {
    it("Verify the following elements are present on the login page", function() {
      login.initialPresentElements();
    });
  });

  describe("#Case 2", function() {
    it("Verify the following additional elements are present on the login page after a correct user address has been entered: authentication type drop-down list, password.", function() {
      login
        .setEmail("saraygomezmartin@gmail.com")
        .goNext()
        .isPassEnabled()
        .checkTextFromLoginAsDifferentUser()
        .checkTextFromForgotPassword();
    });

    it("Both of these elements should be enabled and verify the link text is as expected", function() {
      login
      .checkTextFromLoginAsDifferentUser()
      .checkTextFromForgotPassword();
    });

    it("Verify that the ‘Login as a different user’ button returns the user to the initial login page and that the link text is as expected", function() {
      login
      .loginAsDifferentUser()
      .initialPresentElements();
    });

    it("Verify that that the reset password navigates the user to the reset page, and that the following elements, all enabled, are present on the reset page: Email address to reset, reset button, ‘never mind take me back’ link.", function() {
      login
        .setEmail("cocoatechdesign@gmail.com")
        .goNext()
        .forgotPassword()
        .checkTextFromForgotPasswordView();
    });

    it("Verify the link text is as expected (the app text is the correct text). Verify that the ‘Never mind’ link takes the user back to the login page with the Password element present and enabled, and the Login button present and disabled.", function() {
      login
        .checkNeverMindLinkText()
        .neverMindReturnToLogin()
        .isLoginButtonDisabled();
    });
  });

  describe("#Case 3", function() {
    beforeEach(function() {
      browser.get(URL);
    });
    it("Verify that the user cannot log into the Secure Messaging site with an invalid user address. (error text – match expected text message)", function() {
       login
        .setEmail("sda@gmail.com")
        .goNext()
        .setPass("cwQ-4ue3t74b4C^Gbg43e$")
        .pressLogin()
        .checkErrorLoginMessage();
    });
  });

  describe("#Case 4", function() {
    beforeEach(function() {
      browser.get(URL);
    });
    it("Verify that the user cannot log into the Secure Messaging site with an invalid password. (error text – match expected text message)", function() {
       login
        .setEmail("saraygomezmartin@gmail.com")
        .goNext()
        .setPass("12345.Passw0rd")
        .pressLogin()
        .checkErrorLoginMessage();
    });
  });

  describe("#Case 5", function() {
    beforeEach(function() {
      browser.get(URL);
    });
    it("Verify that the user can login with correct user address and password (I.e. Your address and your changed password). Verify that e.g. The inbox refresh button is present and visible, in order to do this.", function() {
       login
        .setEmail("saraygomezmartin@gmail.com")
        .goNext()
        .setPass("cwQ-4ue3t74b4C^Gbg43e$")
        .pressLogin();
        
        inbox
        .isRefreshInboxDisplayed();
    });
  });

   describe("#Case 6", function() {
    beforeEach(function() {
      browser.get(URL);
    });
    it("Verify that the user, when logged in, can compose and send a message.", function() {
       login
        .setEmail("saraygomezmartin@gmail.com")
        .goNext()
        .setPass("cwQ-4ue3t74b4C^Gbg43e$")
        .pressLogin();
        
        inbox
        .createNewEmail()
        .sentTo("saraygomezmartin@gmail.com")
        .withSubject("This is Protractor!")
        .andContent("Alohamora wand elf parchment, Wingardium Leviosa hippogriff, house dementors betrayal. Holly, Snape centaur portkey ghost Hermione spell bezoar Scabbers. Peruvian-Night-Powder werewolf, Dobby pear-tickle half-moon-glasses, Knight-Bus.");
    });
  });

  // END DESCRIBE
});
