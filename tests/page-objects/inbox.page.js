"use strict";

var InboxPage = function() {
  this.refreshButton = element(by.css(".icon-arrows-ccw"));
  this.newEmail = element(by.css('[ng-click="action()"]'));
  this.emailTo = element(by.css('[.selectize-input"]'));
  this.emailSubject = element(by.css('[ng-model="email.subject"]'));
  this.emailContent = element(by.css(".note-editable"));

  this.isRefreshInboxDisplayed = function() {
    expect(this.refreshButton.isDisplayed()).toBeTruthy();
    expect(this.refreshButton.isEnabled()).toBeTruthy();
    return this;
  };

  this.refreshInbox = function() {
    expect(this.refreshButton.isDisplayed()).toBeTruthy();
    this.refreshButton.click();
    return this;
  };

  this.createNewEmail = function() {
    expect(this.newEmail.isDisplayed()).toBeTruthy();
    this.newEmail.click();
    return this;
  };

  this.sentTo = function(to) {
    expect(this.emailDestination.isDisplayed()).toBeTruthy();
    this.emailDestination.sendKeys(to);
    return this;
  };

  this.withSubject = function(subject) {
    expect(this.emailSubject.isDisplayed()).toBeTruthy();
    this.emailSubject.sendKeys(subject);
    return this;
  };

  this.andContent = function(content) {
    expect(this.emailContent.isDisplayed()).toBeTruthy();
    this.emailContent.sendKeys(content);
    return this;
  };
};
module.exports = InboxPage;
