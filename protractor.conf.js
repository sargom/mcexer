var SpecReporter = require("jasmine-spec-reporter").SpecReporter;

exports.config = {
  directConnect: true,
  seleniumAddress: "http://localhost:4444/wd/hub",
  capabilities: {
    browserName: "chrome"
  },
  baseURL: "https://login-alpha.mimecast.com/m/secure/login",
  specs: ["tests/authentication.js"],
  framework: "jasmine",
  jasmineNodeOpts: {
    showColors: true,
    isVerbose: true,
    realtimeFailure: true,
    includeStackTrace: true,
    defaultTimeoutInterval: 30000
  },

  onPrepare: function() {
    browser.driver.manage().window().maximize();

    jasmine.getEnv().addReporter(
      new SpecReporter({
        spec: {
          displayStacktrace: false
        }
      })
    );
  }
};
