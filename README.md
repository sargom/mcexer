# Setup #
Please follow the next steps to setup the project:

`````
$ git clone https://sargom@bitbucket.org/sargom/mcexer.git
$ cd mcexerc
$ npm run setup
`````

# Launch the Test #

`````
$ npm run test
`````