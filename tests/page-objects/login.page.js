"use strict";

var LoginPage = function() {
  browser.get("https://login-alpha.mimecast.com/m/secure/login/?tkn=WVXYHQ7cA8nSKgxK0RS4nkdOIDqpeB5cYTr3ptKQOtRRUm0poN7PZ_cOXXqmu1ZSPxbDSpccdqyHcLr4Z0WYoUUaJR4MrVGHESpRXK3_Gyw#/login?message=eNo1jm1LwzAURv9Lvrpo0qRZO0ScuoEWRQYqDKGk6W2tzYukacGK_91s6ufnnnPuFxpAjR66Gq0QqIFs2kk-Wr1liZ-9cVSQ4vbGz7naUr1fa1UnxXsfqs3VJHfu2e3h5Y41iX09q0-sRQtkYBhkC9q5fvwIrgeLVnbUeoGCCQ-uhtghhMZLqUI3_Yav6ZqWOeVZJvLynDecsIqkOBGsxryRFa6YYBikUHGTXDZwEQUT-KFz0U-P8ns32oNs7AN4Qw4Fr97-E5td-VTwFNOsPNYSQpeE07QU5R-A41s4Z0sqch5hMLLTERykl5-tMzAb6UNnL9vDcKqcQd8_rKRkZQ");

  this.emailInput = element(by.id("username"));
  this.passwordInput = element(by.css('[ng-model="appCtrl.password"]'));
  this.nextButton = element(by.css('[ng-click="appCtrl.stepOne()"]'));
  this.loginDifferentUserLink = element(by.css('[ng-click="appCtrl.reset()"]'));
  this.forgotPasswordLink = element(by.css('button[ui-sref="forgot-password"]'));
  this.resetPassword = element(by.css('[ng-click="forgotPasswordControllerCtrl.requestPasswordReset()"]'));
  this.neverMindReturnToLoginLink = element(by.css('button[ui-sref="login"]'));
  this.loginButton = element(by.css('button[ng-click="appCtrl.submit()"]'));
  this.errorLoginMessage = element(by.binding("appCtrl.errorMessage"));

  this.initialPresentElements = function() {
    expect(this.emailInput.isDisplayed()).toBeTruthy().then(function() {
      console.log("INFO: Email address is Displayed");
    });

    expect(this.nextButton.isDisplayed()).toBeTruthy().then(function() {
      console.log("INFO: Next is Displayed");
    });

    expect(this.emailInput.isEnabled()).toBeTruthy().then(function() {
      console.log("INFO: Email address is Enabled");
    });

    expect(this.nextButton.isEnabled()).toBeFalsy().then(function() {
      console.log("INFO: Next is not Enabled");
    });
    return this;
  };

  this.isEmailEnabled = function() {
    expect(this.emailInput.isDisplayed()).toBeTruthy();
    expect(this.emailInput.isEnabled()).toBeTruthy();
    return this;
  };

  this.isNextEnabled = function() {
    expect(this.nextButton.isDisplayed()).toBeTruthy();
    expect(this.nextButton.isEnabled()).toBeTruthy();
    return this;
  };

  this.isPassEnabled = function() {
    expect(this.passwordInput.isDisplayed()).toBeTruthy();
    expect(this.passwordInput.isEnabled()).toBeTruthy();
    return this;
  };

  this.isLoginButtonDisabled = function() {
    expect(this.loginButton.isDisplayed()).toBeTruthy();
    expect(this.loginButton.isEnabled()).toBeFalsy();
    return this;
  };

  this.setEmail = function(email) {
    this.emailInput.sendKeys(email);
    return this;
  };

  this.setPass = function(pass) {
    this.isPassEnabled();
    this.passwordInput.sendKeys(pass);
    return this;
  };

  this.goNext = function() {
    this.isNextEnabled();
    this.nextButton.click();
    return this;
  };

    this.pressLogin = function() {
    this.loginButton.click();
    return this;
  };


  this.checkTextFromLoginAsDifferentUser = function() {
    expect(this.loginDifferentUserLink.isDisplayed()).toBeTruthy();
    expect(this.loginDifferentUserLink.getText()).toBe(
      "Log in as a different user."
    );
    return this;
  };

  this.checkTextFromForgotPassword = function() {
    expect(this.forgotPasswordLink.isDisplayed()).toBeTruthy();
    expect(this.forgotPasswordLink.getText()).toBe("Forgot your password?");
    return this;
  };

  this.loginAsDifferentUser = function() {
    expect(this.loginDifferentUserLink.isDisplayed()).toBeTruthy();
    this.loginDifferentUserLink.click();
    return this;
  };

  this.forgotPassword = function() {
    expect(this.forgotPasswordLink.isDisplayed()).toBeTruthy();
    this.forgotPasswordLink.click();
    return this;
  };

  this.checkTextFromForgotPasswordView = function() {
    this.isEmailEnabled();
    expect(this.resetPassword.isDisplayed()).toBeTruthy();
    expect(this.resetPassword.isEnabled()).toBeTruthy();
    expect(this.neverMindReturnToLoginLink.isDisplayed()).toBeTruthy();
    expect(this.neverMindReturnToLoginLink.isEnabled()).toBeTruthy();
    return this;
  };

  this.checkNeverMindLinkText = function() {
    expect(this.neverMindReturnToLoginLink.isDisplayed()).toBeTruthy();
    expect(this.neverMindReturnToLoginLink.getText()).toBe(
      "Never mind, take me back to the login page."
    );
    return this;
  };

  this.neverMindReturnToLogin = function() {
    expect(this.neverMindReturnToLoginLink.isDisplayed()).toBeTruthy();
    this.neverMindReturnToLoginLink.click();
    return this;
  };

  this.checkErrorLoginMessage = function() {
    expect(this.errorLoginMessage.isDisplayed()).toBeTruthy();
    expect(this.errorLoginMessage.getText()).toBe(
      "Invalid user name, password or permissions."
    );
    return this;
  };
};
module.exports = LoginPage;
